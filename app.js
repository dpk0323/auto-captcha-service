const express = require('express')
const utils = require('./main.js')
const app = express()
const bodyParser = require('body-parser')
const fs = require('fs')
const port = 3000

let data = {}

app.use(bodyParser.json())

app.use(function (req, res, next) {
  res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
  next();
});

app.get('/healthcheck', async (req, res) => {
  res.status(200).send('OK');
})


app.get('/generate_captcha', async (req, res) => {
  try {
    let captchaData = await utils.getCaptcha()
    data[ captchaData.sessId ] = captchaData
    res.json({
      id: captchaData.sessId,
      img: captchaData.img
    })
  }
  catch (e) {
    return res.status(400).send('There was an error. Please Try Again')
  }
})

app.get('/auto_captcha/v1/generate_captcha', async (req, res) => {
  try {
    let captchaData = await utils.getCaptcha()
    data[ captchaData.sessId ] = captchaData
    res.json({
      id: captchaData.sessId,
      img: captchaData.img
    })
  }
  catch (e) {
    return res.status(400).send('There was an error. Please Try Again')
  }
})

app.post('/auto_captcha/v1/fill_captcha', async (req, res) => {
  try {
    let resp = await utils.fillForm(data[ req.body.id ], req.body.captcha, req.body.email, req.body.pan)
    if (data.hasOwnProperty(req.body.id)) {
      delete data[ req.body.id ]
    }
    else {
      return res.status(400).send('Invalid ID')
    }
    if (resp.statusCode == 302) {
      res.send('Success')
    }
    else if (resp.statusCode == 200) {
      res.status(400).send('The Captcha Entered is Wrong. Please Try Again.')
    }
    else {
      res.status(400).send('There was an error. Please Try Again')
    }
  }
  catch (err) {
    return res.status(400).send('There was an error. Please Try Again')
  }
})

app.post('/fill_captcha', async (req, res) => {
  try {
    let resp = await utils.fillForm(data[ req.body.id ], req.body.captcha, req.body.email, req.body.pan)
    if (data.hasOwnProperty(req.body.id)) {
      delete data[ req.body.id ]
    }
    else {
      return res.status(400).send('Invalid ID')
    }
    if (resp.statusCode == 302) {
      res.send('Success')
    }
    else if (resp.statusCode == 200) {
      res.status(400).send('The Captcha Entered is Wrong. Please Try Again.')
    }
    else {
      res.status(400).send('There was an error. Please Try Again')
    }
  }
  catch (err) {
    return res.status(400).send('There was an error. Please Try Again')
  }
})

console.log("Getting Initial Cookies...")
utils.getInitialAuthCookies().then(res => {
  app.listen(port, () => console.log(`Example app listening at http://localhost:${port}`))
}).catch(err => {
  console.error("There was an error getting initial cookies")
  console.error(e)
})
