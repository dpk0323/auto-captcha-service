const rp = require('request-promise');
const cheerio = require('cheerio');
const fs = require('fs')
const fakeUa = require('fake-useragent');
const request = require('request')

let baseUrl = 'www.camsonline.com'

let cookies = {
  'AuthenticationTicket':'',
  'ASP.NET_SessionId':''
}

async function sendCaptcha(data,captcha,email,pan){
  let {formData,sessCookie,agent} = data
  let path = '/InvestorServices/COL_ISAccountStatementCKF.aspx'
  let url = "https://"+baseUrl+path
  let jar = getCookieJar(url)
  jar.setCookie(sessCookie,url)

  let body = {}
  formData.forEach(input=>{
    if(input.name == 'ctl00$PageContent$txtemailzyx'){
      body[input.name] = email
    }
    else if(input.name=='ctl00$PageContent$txtCaptcha'){
      body[input.name] = captcha
    }
    else if(input.name=='ctl00$PageContent$txtPANMMDNo'){
      body[input.name] = pan
    }
    else {
      body[input.name] = input.value
    }
  })
  body['ctl00$PageContent$btnsubmit.x'] = 59
  body['ctl00$PageContent$btnsubmit.y'] = 14
  let options = {
    url:url,
    jar:jar,
    method:'POST',
    resolveWithFullResponse:true,
    headers:{
      'User-Agent':agent,
      'Content-Type':'application/x-www-form-urlencoded; charset=UTF-8',
    },
    form:body,
    simple: false
  }
  let resp = await rp(options)
  return(resp)
}

async function getCaptchaScraping(){
  let path = '/InvestorServices/COL_ISAccountStatementCKF.aspx'
  let url = "https://"+baseUrl+path
  let jar = getCookieJar(url)
  let err = true
  let agent = fakeUa()
  let options = {
    url:url,
    jar:jar,
    method:'GET',
    resolveWithFullResponse:true,
    headers:{
      'User-Agent':agent
    }
  }

  let resp = await rp(options)
  let sessCookie = rp.cookie(resp.headers['set-cookie'][0].split(';')[0])
  let $ = cheerio.load(resp.body)
  jar.setCookie(sessCookie,url)

  //Filling most of the form
  let temp = $('input[name="ctl00$PageContent$hdn1"]').attr('value')
  $('input[name="ctl00$PageContent$txtemailzyx"]').attr('value','')
  $('input[name="ctl00$PageContent$rdSttype"]').attr('value','D')
  $('input[name="ctl00$PageContent$Period"]').attr('value','radAnyDate')
  $('input[name="ctl00$PageContent$txtFromutrDt"]').attr('value','01/01/1990')
  $('input[name="ctl00$PageContent$txtPANMMDNo"]').attr('value','')
  $('input[name="ctl00$PageContent$txtemyPassword"]').attr('value','Groww123')
  $('input[name="ctl00$PageContent$txtRetypePassword"]').attr('value','Groww123')
  $('input[name="ctl00$PageContent$hdn2"]').attr('value',temp[0])
  $('input[name="ctl00$PageContent$hdn3"]').attr('value',temp[1])
  $('input[name="ctl00$PageContent$hdn4"]').attr('value',temp[2])

  let sessId = $('input[name="ctl00$PageContent$hidSess"]').attr('value')
  let imgBase64 = await saveCaptcha(sessId,sessCookie,agent)
  
  return {
    img:imgBase64,
    formData:$('#form1').serializeArray(),
    sessCookie:sessCookie,
    agent:agent,
    sessId:sessId
  }
}

getCookieJar = (url)=>{
  let jar = rp.jar()
  let cookie = rp.cookie(`AuthenticationTicket=${cookies.AuthenticationTicket}`)
  jar.setCookie(cookie,url)
  cookie = rp.cookie(`ASP.NET_SessionId=${cookies['ASP.NET_SessionId']}`)
  jar.setCookie(cookie,url)
  return jar
}

saveCaptcha = (sessId,sessCookie,agent)=>{
  return new Promise((resolve,reject)=>{
    let url = 'https://'+baseUrl+'/CaptchaHandles.ashx'
    let jar = getCookieJar(url)
    jar.setCookie(sessCookie,url)
    let options = {
      url:url,
      qs:{
        cisno:sessId
      },
      method:'GET',
      jar:jar,
      headers:{
        'User-Agent':agent,
        'Connection':'keep-alive',
        'Referer':url,
        'Accept':'image/webp,image/apng,image/*,*/*;q=0.8'
      },
      resolveWithFullResponse:false
    }
    let imgStream = request(options)
    let chunks = []
    imgStream.on('data',(chunk)=>{
      chunks.push(chunk)
    })
    imgStream.on('end',()=>{
      let res = Buffer.concat(chunks)
      resolve(res.toString('base64'))
    })
  })
}

async function getInitialAuthCookies(){
  let agent = fakeUa()
  let url = "https://"+baseUrl+"/CAMS_Consent.aspx"
  let options = {
    url:url,
    method:'GET',
    resolveWithFullResponse:true,
    headers:{
      'User-Agent':agent
    }
  }

  let resp = await rp(options)
  let $ = cheerio.load(resp.body)
  let body = {}
  $('#form1').serializeArray().forEach(input=>{
    body[input.name] = input.value
  })
  body['Proceed'] = 'rdo_accept'
  body['btn_Proceed'] = 'PROCEED'
  options = {
    url:url,
    method:'POST',
    resolveWithFullResponse:true,
    headers:{
      'User-Agent':agent,
      'Content-Type':'application/x-www-form-urlencoded; charset=UTF-8',
    },
    form:body,
    simple:false
  }
  resp = await rp(options)
  resp.headers['set-cookie'].forEach(cookie=>{
    cookies[cookie.split(';')[0].split('=')[0]] = cookie.split(';')[0].split('=')[1]
  })
  return true
}

module.exports = {
  getCaptcha:getCaptchaScraping,
  fillForm:sendCaptcha,
  getInitialAuthCookies:getInitialAuthCookies
}


