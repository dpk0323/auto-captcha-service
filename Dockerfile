ARG BASE_IMAGE
FROM $BASE_IMAGE

RUN mkdir -p /usr/share/auto-captcha-service
COPY . /usr/share/auto-captcha-service
WORKDIR /usr/share/auto-captcha-service
EXPOSE 3000
CMD ["sh", "-c", "./start.sh"]

